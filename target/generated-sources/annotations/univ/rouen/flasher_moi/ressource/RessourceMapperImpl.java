package univ.rouen.flasher_moi.ressource;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import univ.rouen.flasher_moi.anomalie.Anomalie;
import univ.rouen.flasher_moi.ressource.Ressource.RessourceBuilder;
import univ.rouen.flasher_moi.ressource.RessourceDTO.RessourceDTOBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-26T21:05:46+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (JetBrains s.r.o.)"
)
@Component
public class RessourceMapperImpl implements RessourceMapper {

    @Override
    public Ressource map(RessourceDTO ressourceDTO) {
        if ( ressourceDTO == null ) {
            return null;
        }

        RessourceBuilder ressource = Ressource.builder();

        ressource.id( ressourceDTO.getId() );
        ressource.description( ressourceDTO.getDescription() );
        ressource.localisation( ressourceDTO.getLocalisation() );
        ressource.qrCode( ressourceDTO.getQrCode() );
        List<Anomalie> list = ressourceDTO.getAnomalies();
        if ( list != null ) {
            ressource.anomalies( new ArrayList<Anomalie>( list ) );
        }

        return ressource.build();
    }

    @Override
    public RessourceDTO map(Ressource ressourceDTO) {
        if ( ressourceDTO == null ) {
            return null;
        }

        RessourceDTOBuilder ressourceDTO1 = RessourceDTO.builder();

        ressourceDTO1.id( ressourceDTO.getId() );
        ressourceDTO1.description( ressourceDTO.getDescription() );
        ressourceDTO1.localisation( ressourceDTO.getLocalisation() );
        ressourceDTO1.qrCode( ressourceDTO.getQrCode() );

        return ressourceDTO1.build();
    }

    @Override
    public List<RessourceDTO> map(List<Ressource> ressources) {
        if ( ressources == null ) {
            return null;
        }

        List<RessourceDTO> list = new ArrayList<RessourceDTO>( ressources.size() );
        for ( Ressource ressource : ressources ) {
            list.add( map( ressource ) );
        }

        return list;
    }

    @Override
    public void updateEntity(RessourceDTO ressourceDTO, Ressource ressource) {
        if ( ressourceDTO == null ) {
            return;
        }

        ressource.setId( ressourceDTO.getId() );
        ressource.setDescription( ressourceDTO.getDescription() );
        ressource.setLocalisation( ressourceDTO.getLocalisation() );
        ressource.setQrCode( ressourceDTO.getQrCode() );
        if ( ressource.getAnomalies() != null ) {
            List<Anomalie> list = ressourceDTO.getAnomalies();
            if ( list != null ) {
                ressource.getAnomalies().clear();
                ressource.getAnomalies().addAll( list );
            }
            else {
                ressource.setAnomalies( null );
            }
        }
        else {
            List<Anomalie> list = ressourceDTO.getAnomalies();
            if ( list != null ) {
                ressource.setAnomalies( new ArrayList<Anomalie>( list ) );
            }
        }
    }
}
