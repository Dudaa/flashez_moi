package univ.rouen.flasher_moi.anomalie;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import univ.rouen.flasher_moi.anomalie.Anomalie.AnomalieBuilder;
import univ.rouen.flasher_moi.anomalie.AnomalieDTO.AnomalieDTOBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-26T21:05:46+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (JetBrains s.r.o.)"
)
@Component
public class AnomalieMapperImpl implements AnomalieMapper {

    @Override
    public Anomalie map(AnomalieDTO anomalieDTO) {
        if ( anomalieDTO == null ) {
            return null;
        }

        AnomalieBuilder anomalie = Anomalie.builder();

        anomalie.id( anomalieDTO.getId() );
        anomalie.description( anomalieDTO.getDescription() );
        anomalie.traite( anomalieDTO.isTraite() );

        return anomalie.build();
    }

    @Override
    public AnomalieDTO map(Anomalie anomalie) {
        if ( anomalie == null ) {
            return null;
        }

        AnomalieDTOBuilder anomalieDTO = AnomalieDTO.builder();

        anomalieDTO.id( anomalie.getId() );
        anomalieDTO.description( anomalie.getDescription() );
        anomalieDTO.traite( anomalie.isTraite() );

        return anomalieDTO.build();
    }

    @Override
    public List<AnomalieDTO> map(List<Anomalie> anomalies) {
        if ( anomalies == null ) {
            return null;
        }

        List<AnomalieDTO> list = new ArrayList<AnomalieDTO>( anomalies.size() );
        for ( Anomalie anomalie : anomalies ) {
            list.add( map( anomalie ) );
        }

        return list;
    }

    @Override
    public void updateEntity(AnomalieDTO anomalieDTO, Anomalie anomalie) {
        if ( anomalieDTO == null ) {
            return;
        }

        anomalie.setId( anomalieDTO.getId() );
        anomalie.setDescription( anomalieDTO.getDescription() );
        anomalie.setTraite( anomalieDTO.isTraite() );
    }
}
