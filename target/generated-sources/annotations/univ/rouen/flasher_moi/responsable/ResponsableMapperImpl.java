package univ.rouen.flasher_moi.responsable;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;
import univ.rouen.flasher_moi.anomalie.Anomalie;
import univ.rouen.flasher_moi.ressource.Ressource;
import univ.rouen.flasher_moi.ressource.Ressource.RessourceBuilder;
import univ.rouen.flasher_moi.ressource.RessourceDTO;
import univ.rouen.flasher_moi.ressource.RessourceDTO.RessourceDTOBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-12-26T21:05:46+0100",
    comments = "version: 1.3.0.Final, compiler: javac, environment: Java 11.0.7 (JetBrains s.r.o.)"
)
@Component
public class ResponsableMapperImpl implements ResponsableMapper {

    @Override
    public ResponsableDTO map(Responsable responsable) {
        if ( responsable == null ) {
            return null;
        }

        ResponsableDTO responsableDTO = new ResponsableDTO();

        responsableDTO.setId( responsable.getId() );
        responsableDTO.setLastName( responsable.getLastName() );
        responsableDTO.setFirstName( responsable.getFirstName() );
        responsableDTO.setEmail( responsable.getEmail() );
        responsableDTO.setPassword( responsable.getPassword() );
        responsableDTO.setRessources( ressourceListToRessourceDTOList( responsable.getRessources() ) );

        return responsableDTO;
    }

    @Override
    public Responsable map(ResponsableDTO responsableDTO) {
        if ( responsableDTO == null ) {
            return null;
        }

        Responsable responsable = new Responsable();

        responsable.setId( responsableDTO.getId() );
        responsable.setLastName( responsableDTO.getLastName() );
        responsable.setFirstName( responsableDTO.getFirstName() );
        responsable.setEmail( responsableDTO.getEmail() );
        responsable.setPassword( responsableDTO.getPassword() );
        responsable.setRessources( ressourceDTOListToRessourceList( responsableDTO.getRessources() ) );

        return responsable;
    }

    @Override
    public List<ResponsableDTO> map(List<Responsable> responsables) {
        if ( responsables == null ) {
            return null;
        }

        List<ResponsableDTO> list = new ArrayList<ResponsableDTO>( responsables.size() );
        for ( Responsable responsable : responsables ) {
            list.add( map( responsable ) );
        }

        return list;
    }

    protected RessourceDTO ressourceToRessourceDTO(Ressource ressource) {
        if ( ressource == null ) {
            return null;
        }

        RessourceDTOBuilder ressourceDTO = RessourceDTO.builder();

        ressourceDTO.id( ressource.getId() );
        ressourceDTO.description( ressource.getDescription() );
        ressourceDTO.localisation( ressource.getLocalisation() );
        ressourceDTO.qrCode( ressource.getQrCode() );
        List<Anomalie> list = ressource.getAnomalies();
        if ( list != null ) {
            ressourceDTO.anomalies( new ArrayList<Anomalie>( list ) );
        }

        return ressourceDTO.build();
    }

    protected List<RessourceDTO> ressourceListToRessourceDTOList(List<Ressource> list) {
        if ( list == null ) {
            return null;
        }

        List<RessourceDTO> list1 = new ArrayList<RessourceDTO>( list.size() );
        for ( Ressource ressource : list ) {
            list1.add( ressourceToRessourceDTO( ressource ) );
        }

        return list1;
    }

    protected Ressource ressourceDTOToRessource(RessourceDTO ressourceDTO) {
        if ( ressourceDTO == null ) {
            return null;
        }

        RessourceBuilder ressource = Ressource.builder();

        ressource.id( ressourceDTO.getId() );
        ressource.description( ressourceDTO.getDescription() );
        ressource.localisation( ressourceDTO.getLocalisation() );
        ressource.qrCode( ressourceDTO.getQrCode() );
        List<Anomalie> list = ressourceDTO.getAnomalies();
        if ( list != null ) {
            ressource.anomalies( new ArrayList<Anomalie>( list ) );
        }

        return ressource.build();
    }

    protected List<Ressource> ressourceDTOListToRessourceList(List<RessourceDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Ressource> list1 = new ArrayList<Ressource>( list.size() );
        for ( RessourceDTO ressourceDTO : list ) {
            list1.add( ressourceDTOToRessource( ressourceDTO ) );
        }

        return list1;
    }
}
