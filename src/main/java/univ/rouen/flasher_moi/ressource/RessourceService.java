package univ.rouen.flasher_moi.ressource;

import org.springframework.stereotype.Service;
import univ.rouen.flasher_moi.anomalie.Anomalie;
import univ.rouen.flasher_moi.anomalie.AnomalieDTO;
import univ.rouen.flasher_moi.anomalie.AnomalieMapper;

import java.util.List;
import java.util.Optional;

@Service
public class RessourceService {
    private final RessourceRepository ressourceRepository;
    private final RessourceMapper ressourceMapper;
    private final AnomalieMapper anomalieMapper;

    public RessourceService(RessourceRepository ressourceRepository, RessourceMapper ressourceMapper, AnomalieMapper anomalieMapper) {
        this.ressourceRepository = ressourceRepository;
        this.ressourceMapper = ressourceMapper;
        this.anomalieMapper = anomalieMapper;
    }

    void add(RessourceDTO ressourceDTO) {
        Ressource ressource = ressourceMapper.map(ressourceDTO);
        ressourceRepository.save(ressource);
    }

    List<RessourceDTO> list() {
        return ressourceMapper.map(ressourceRepository.findAll());
    }

    List<AnomalieDTO> getRessourceAnomalies(Long idRessource) {
        List<Anomalie> anomalies = ressourceRepository.getRessourceAnomalies(idRessource);
        return anomalieMapper.map(anomalies);
    }

    void update(RessourceDTO ressourceDTO) {
        Optional<Ressource> ressourceToUpdate = ressourceRepository.findById(ressourceDTO.getId());
        if (ressourceToUpdate.isPresent()) {
            ressourceMapper.updateEntity(ressourceDTO, ressourceToUpdate.get());
            ressourceRepository.save(ressourceToUpdate.get());
        }
    }

}
