package univ.rouen.flasher_moi.ressource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import univ.rouen.flasher_moi.anomalie.Anomalie;

import javax.persistence.OneToMany;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RessourceDTO {
    private Long id;
    private String description;
    private String localisation;
    private String qrCode;
    private List<Anomalie> anomalies;
}
