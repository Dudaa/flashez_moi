package univ.rouen.flasher_moi.ressource;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;


import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RessourceMapper {
    Ressource map(RessourceDTO ressourceDTO);

    @Mapping(source = "anomalies", target = "anomalies", ignore = true)
    RessourceDTO map(Ressource ressourceDTO);

    List<RessourceDTO> map(List<Ressource> ressources);

    void updateEntity(RessourceDTO ressourceDTO, @MappingTarget Ressource ressource);


}
