package univ.rouen.flasher_moi.ressource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import univ.rouen.flasher_moi.anomalie.AnomalieDTO;

import java.util.List;

@RestController
@RequestMapping("ressources")
public class RessourceController {
    private final RessourceService service;

    public RessourceController(RessourceService service) {
        this.service = service;
    }

    @PostMapping
    public void add(@RequestBody RessourceDTO ressourceDTO) {
        service.add(ressourceDTO);
    }

  /*  @GetMapping
    public List<RessourceDTO> list() {
        return service.list();
    }

    @GetMapping("{idRessource}")
    public List<AnomalieDTO> getRessourceAnomalies(@PathVariable("idRessource") Long idRessource) {
        return service.getRessourceAnomalies(idRessource);
    }*/

    @PutMapping
    public void update(@RequestBody RessourceDTO ressourceDTO) {
        service.update(ressourceDTO);
    }

    @GetMapping
    public Ressource testAPI() {
        return Ressource.builder().id(2L).description("test").build();
    }
}
