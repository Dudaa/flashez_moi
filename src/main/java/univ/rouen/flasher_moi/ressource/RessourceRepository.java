package univ.rouen.flasher_moi.ressource;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import univ.rouen.flasher_moi.anomalie.Anomalie;

import java.util.List;

public interface RessourceRepository extends JpaRepository<Ressource, Long> {
    @Query(value = "SELECT r.anomalies FROM Ressource r where r.id=?1", nativeQuery = true)
    List<Anomalie> getRessourceAnomalies(Long idRessource);
}
