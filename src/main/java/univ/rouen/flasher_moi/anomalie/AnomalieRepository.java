package univ.rouen.flasher_moi.anomalie;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnomalieRepository extends JpaRepository<Anomalie,Long> {
}
