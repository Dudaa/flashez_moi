package univ.rouen.flasher_moi.anomalie;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AnomalieMapper {
    Anomalie map(AnomalieDTO anomalieDTO);

    AnomalieDTO map(Anomalie anomalie);

    List<AnomalieDTO> map(List<Anomalie> anomalies);

    void updateEntity(AnomalieDTO anomalieDTO, @MappingTarget Anomalie anomalie);
}
