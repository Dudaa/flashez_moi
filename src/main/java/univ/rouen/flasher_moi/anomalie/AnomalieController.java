package univ.rouen.flasher_moi.anomalie;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/anomalies")
public class AnomalieController {
    private final AnomalieService anomalieService;

    public AnomalieController(AnomalieService anomalieService) {
        this.anomalieService = anomalieService;
    }

    @PostMapping
    public void add(@RequestBody AnomalieDTO anomalieDTO) {
        anomalieService.add(anomalieDTO);
    }

    @PutMapping
    public AnomalieDTO update(@RequestBody AnomalieDTO anomalieDTO) {
        return anomalieService.update(anomalieDTO);
    }

    @GetMapping
    public List<AnomalieDTO> list() {
        return anomalieService.list();
    }

    @PutMapping("{idAnomalie}")
    public void resolve(@PathVariable("idAnomalie") Long idAnomalie) {
        anomalieService.resolve(idAnomalie);
    }
}
