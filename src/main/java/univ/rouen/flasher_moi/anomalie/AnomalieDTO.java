package univ.rouen.flasher_moi.anomalie;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnomalieDTO {
    private Long id;
    private String description;
    private boolean traite;
}
