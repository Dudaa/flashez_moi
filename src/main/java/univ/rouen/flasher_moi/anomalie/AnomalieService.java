package univ.rouen.flasher_moi.anomalie;

import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnomalieService {
    private final AnomalieRepository anomalieRepository;
    private final AnomalieMapper anomalieMapper;

    public AnomalieService(AnomalieRepository anomalieRepository, AnomalieMapper anomalieMapper) {
        this.anomalieRepository = anomalieRepository;
        this.anomalieMapper = anomalieMapper;
    }

    void add(AnomalieDTO anomalieDTO) {
        Anomalie anomalie = anomalieMapper.map(anomalieDTO);
        anomalieRepository.save(anomalie);
    }

    AnomalieDTO update(AnomalieDTO anomalieDTO) {
        Optional<Anomalie> anomalie = anomalieRepository.findById(anomalieDTO.getId());
        if (anomalie.isPresent()) {
            anomalieMapper.updateEntity(anomalieDTO, anomalie.get());
            return anomalieMapper.map(anomalieRepository.save(anomalie.get()));
        }
        throw new ObjectNotFoundException(anomalieDTO.getId(), "anomalie non trouvé");
    }

    public List<AnomalieDTO> list() {
        return anomalieMapper.map(anomalieRepository.findAll());
    }

    public void resolve(Long idAnomalie) {
        Optional<Anomalie> anomalie = anomalieRepository.findById(idAnomalie);
        if (anomalie.isPresent()) {
            Anomalie anomalieToResolve = anomalie.get();
            anomalieToResolve.setTraite(true);
            anomalieRepository.save(anomalieToResolve);
        }

    }
}
