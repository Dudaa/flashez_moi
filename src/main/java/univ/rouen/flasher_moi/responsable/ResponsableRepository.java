package univ.rouen.flasher_moi.responsable;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsableRepository  extends JpaRepository<Responsable,Long> {
}
