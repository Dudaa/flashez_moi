package univ.rouen.flasher_moi.responsable;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponsableService {
    private final ResponsableRepository responsableRepository;
    private final ResponsableMapper responsableMapper;

    public ResponsableService(ResponsableRepository responsableRepository, ResponsableMapper responsableMapper) {
        this.responsableRepository = responsableRepository;
        this.responsableMapper = responsableMapper;
    }

    List<ResponsableDTO> list(){
        return responsableMapper.map(responsableRepository.findAll());
    }
}
