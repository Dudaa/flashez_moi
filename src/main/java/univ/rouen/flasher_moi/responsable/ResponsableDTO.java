package univ.rouen.flasher_moi.responsable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import univ.rouen.flasher_moi.auth.UserDTO;
import univ.rouen.flasher_moi.ressource.RessourceDTO;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponsableDTO extends UserDTO {
    private List<RessourceDTO> ressources;

}
