package univ.rouen.flasher_moi.responsable;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import univ.rouen.flasher_moi.auth.UserMapper;

import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ResponsableMapper {
    ResponsableDTO map(Responsable responsable);

    Responsable map(ResponsableDTO responsableDTO);

    List<ResponsableDTO> map(List<Responsable> responsables);

}
