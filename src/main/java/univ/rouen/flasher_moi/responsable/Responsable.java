package univ.rouen.flasher_moi.responsable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import univ.rouen.flasher_moi.auth.User;
import univ.rouen.flasher_moi.ressource.Ressource;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table
public class Responsable extends User {
    @OneToMany
    private List<Ressource> ressources;
}
